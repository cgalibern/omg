package resappforking

import (
	"opensvc.com/opensvc/core/drivergroup"
	"opensvc.com/opensvc/core/keywords"
	"opensvc.com/opensvc/core/manifest"
	"opensvc.com/opensvc/drivers/resapp"
)

const (
	driverGroup = drivergroup.App
	driverName  = "forking"
)

// Manifest ...
func (t T) Manifest() *manifest.T {
	var keywordL []keywords.Keyword
	keywordL = append(keywordL, resapp.BaseKeywords...)
	keywordL = append(keywordL, resapp.UnixKeywords...)
	m := manifest.New(driverGroup, driverName)
	m.AddContext([]manifest.Context{
		{
			Key:  "path",
			Attr: "Path",
			Ref:  "object.path",
		},
		{
			Key:  "nodes",
			Attr: "Nodes",
			Ref:  "object.nodes",
		},
	}...)
	m.AddKeyword(keywordL...)
	return m
}
