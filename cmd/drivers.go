package cmd

import (
	_ "opensvc.com/opensvc/drivers/resappforking"
	_ "opensvc.com/opensvc/drivers/resappsimple"
	_ "opensvc.com/opensvc/drivers/resfsdir"
	_ "opensvc.com/opensvc/drivers/resfsflag"
	_ "opensvc.com/opensvc/drivers/resiphost"
	_ "opensvc.com/opensvc/drivers/resiproute"
)
